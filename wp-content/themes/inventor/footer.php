<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Inventor
 */
?>
        <div class="copyright-wrapper">
        	<div class="inner">
                <div class="copyright">
                	<p><?php echo esc_attr(get_theme_mod('footer_copy',__('Inventor 2016 | All Rights Reserved.','inventor'))); ?> <?php echo inventor_credit_link(); ?></p>
                </div><!-- copyright --><div class="clear"></div>         
            </div><!-- inner -->
        </div>
    </div>
<?php wp_footer(); ?>
</body>
</html>