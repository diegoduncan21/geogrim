<?php

echo'<div class="container">
        <div class="antecedentes">
            <p data-scrollreveal="enter right after 0s over 3s">
                Desde la creación de GEOGRIM, se ha participado en numerosos proyectos y
                control de obras en las provincias de Formosa, Chaco, Corrientes y Norte de
                Santa Fé, en aspectos vinculados a geotecnia, agrimensura, proyectos
                estructurales y  asesoramiento.
            </p>

            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>EDIFICIOS PUBLICOS:</strong>
                Se han realizado estudios de suelos para grupos de viviendas,
                edificios escolares, hospitales en toda la geografía  
                de la provincia del Chaco y Corrientes, citando las siguientes obras: 
            </p>
            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Centro de transferencias de investigaciones agrarias y agro negocios.Ubicación: 
                Campus Sgto Cabral – Universidad Nacional del Nordeste (UNNE)   Corrientes (Capital).</li>

                <li>Ampliación del Colegio ENS Nº 76 – Ex Colegio Nacional en Resistencia.
                    Comitente: Subsecretaria de Infraestructura Escolar del MECCYT.</li>

                <li>Núcleos Sanitarios en Parque de la Democracia Resistencia. 
                Comitente: MINISTERIO DE OBRAS Y SERVICIOS PUBLICOS DE LA PCIA DEL CHACO.</li>

                <li>Colegio Secundario en Gral Pinedo Chaco Contratista: JAG CONSTRUCCIONES</li>
                <li>E.E Especial Nº 22 en El Sauzalito . Contratista: ROTH CONSTRUCCIONES.</li>
                <li>E.E.T Nº 18 Quitilipi. Contratista: PATAGONIA CONSTRUCCIONES S.R.L.</li>

                <li>Jardín de infantes Nº 125  en Charadai. Contratista: ROTH CONSTRUCCIONES.</li>
                <li>EEP Nº 867 Villa Ángela.  y EGB Nº 418 Villa Ángela Contratista: C.N.G </li>
                <li>Jardín de Infantes a Crear en Hermoso Campo Contratista: C.N.G.</li>
                <li>Centro Educativo de Nivel Terciario N° 51. Contratista: Vial Agro S.R.L</li>

                <li>Nuevo Edificio Hospital Odontológico  Resistencia. Contratista: PATAGONIA CONST.
                Ampliación y refacción del Hospital Salvador Mazza – Villa Ángela. Contratista: C.N.G</li>

                <li>50 Viviendas en Las Breñas – Programa Techo Digno. Contratista: CHACOBRAS S.A.</li>
                <li>50 Viviendas en Las Breñas – Programa Techo Digno. Contratista: CHACOBRAS S.A.</li>
                <li>40 Viviendas en Las Palmas – Programa Techo Digno. Contratista: CHACOBRAS S.A.</li>
                <li>50 Viviendas en Gral Pinedo – Programa Techo Digno. Contratista: ECCSA S.A</li>
                <li>63 Viviendas en Resistencia – Programa Techo Digno. Contratista: ECCSA S.A.</li>
                <li>50 Viviendas en Gral. San Martin – P.T. Digno .Contratista: ECCSA CONSTRUCCIONES</li>
                <li>48 Viviendas Dúplex en Resistencia –P.T. Digno. Contratista: CONST. AVENIDA S.R.L</li>
                <li>25 Viviendas en Villa Ángela – P.T Digno .Contratista: C.N.G.</li>
                <li>25 Viviendas en J.J Castelli – P.T. Digno .Contratista: RUIZ CONSTRUCCIONES</li>
                <li>50 Viviendas en J.J Castelli – P.T. Digno .Contratista: ROTH CONSTRUCCIONES</li>
                <li>30 Viviendas en V° Rio Bermejito – P.T. Digno .Contratista: ROTH CONSTRUCCIONES</li>
                <li>15 Viviendas en Fuerte Esperanza – P.T. Digno .Contratista: RUIZ CONSTRUCCIONES</li>
                <li>40 Viviendas en Napenay – P.T. Digno .Contratista: RUIZ CONSTRUCCIONES</li>
                <li>15 Viviendas en Los Frentones – P.T. Digno. Contratista: NORESCO S.R.L</li>
                <li>20  Viviendas en Puerto Bermejo – P.T. Digno. Contratista: CARRASCO CONST.</li>
            </ul>
            
            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>ANTENA DE COMUNICACIONES, Líneas de Tendido Eléctrico y Transformadoras:</strong>
                Se han realizado estudios de suelos y mediciones de resistividad 
                para líneas de alta, media y baja tensión en  las provincias de Chaco, 
                Corrientes y Formosa y Norte de Santa Fe, pueden citarse las siguientes obras: 
            </p>

            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Antena de Telecomunicaciones en Resistencia. Comitente: CARSA S.A.</li>

                <li>Antena de Telecomunicaciones en Resistencia. Comitente: AMX S.A (CLARO).</li>

                <li>Antena de Telecomunicaciones en Avellada-Recosquista Santa Fe. Comitente: AMX S.A.</li>

                <li>ET y LAT en Lucio V. Mansilla Pcia de Formosa . Contratista.:DISTROCUYO S.A.</li>

                <li>Electrificación Rural en áreas productivas de la provincia del Chaco – 2° Fase – Lote N° 1 –Área Guaycurú. Comitente SECHEEP. Contratista: CHACOBRAS S.A</li>

            </ul>
            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>Comitente: SECRETARIA DE ENERGIA DE CORRIENTES:</strong>
            </p>
            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>1º Etapa: 1º Tramo L.M.T en 33 KV desde  E.T Murucuyá hacia R. prov.   Nº 6 y 13 Murucuyá – Corrientes</li>

                <li>LMT DT 13,2 KV entre ET 132/33/13,2 KV  Goya – Corrientes.</li>

                <li>Fundaciones y trabajos conexos de L.A.T. 132 KV doble terna  "ET Paso de la Patria - ET Pirayú" – Dpto San Cosme –  Corrientes.</li>

                <li>E.T 132/33/12,2  KV – Doble terna “San Luis del Palmar – Santa Ana” - Corrientes.</li>

            </ul>
            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>Comitente: DIRECCION PROVINCIAL  DE ENERGIA DE CORRIENTES (DPEC):</strong>
            </p>
            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Normalización y sistematización de las redes de baja tensión de las localidades e Santo Tome y Alvear”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Santa Lucia, 9 de Julio, Gdor. Martínez, y Pedro R. Fernandez”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Perugorria, Sauce y Pueblo Libertador”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión  en  Corrientes Capital.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Curuzu Cuatia y F. Yofre”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Liegib, Garruchos y Garabí”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Paso de Los Libres, Bompland y Yapeyu”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Monte Caseros y Mocoretá”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Santa Ana y San Luis del Palmar”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Concepción y Tabay”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “San Carlos y Gdor. Virasoro”  - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Ituzaingo, Villa Olivari e Isla Apipe” - Corrientes.</li>

                <li>Normalización y sistematización de las redes de baja tensión de las localidades de “Ituzaingo, Villa Olivari e Isla Apipe”  - Corrientes.</li>

                <li>Nuevo alimentador doble terna L.M.T 33 Kv Libres – Yapeyu.</li>

                <li>Alimentador 33 Kv E.T. "Libres Norte - C.D. Centro y  Distribuidor de  13,2 Kv  E.T. Libres Norte - Distribuidor N°2</li>

                <li>Estudio de Suelos para el Proyecto alimentador doble terna 13,2 KV Colonia Brugne – San Lorenzo.</li>  

                <li>Estudio de Suelos para la construcción de nuevo distribuidor de 13,20 KV y reacondicionamiento de línea existente de 33 a 13,2 KV barrio Laguna Brava Corrientes.</li>

            </ul>

            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>Edificios en altura: pueden citarse los siguientes estudios de suelos:</strong>
            </p>

            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Edificio de 5 Plantas destinado a Departamentos, Locales Comerciales Ubicación: Calles López y Planes y N. Avellaneda P.R Sáenz Peña -  (Chaco). Comitente: BANCO HIPOTECARIO, FIDIUCIARIO FIDEICOMISO Pro.Cre.Ar.</li>

                <li>Edificio de 10 Plantas en avenida Wilde  N° 550 Resistencia (Chaco). – Proyectista, ESTUDIO AD.</li>
            </ul>
            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>Contratista: PATAGONIA CONSTRUCCIONES S.R.L:</strong>
            </p>

            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Nuevo Edificio Hospital Odontológico  Resistencia.</li>
                <li>Edificio de 10 Plantas, cocheras y subsuelo en calle Liniers N° 337 Resistencia.</li>
                <li>Edificio de 7 Plantas, departamentos y subsuelo en calle Ameghino N° 973 Resistencia.</li>
                <li>Edificio de 7 Plantas, dep.y oficina en calle av. Italia y calle G. Montaner Resistencia </li>
                <li>Edificio de 3 Plantas en calle French N° 926 Resistencia (Chaco).</li>
                <li>Edificio de 3 Plantas en calle Alberdi N° 1830 Corrientes (Capital).</li>
            </ul>

            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>Terceros:</strong>
            </p>

            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Edificio de 4 Plantas, locales y departamentos en av. Hernandarias N° 1350  Resistencia. </li>
                <li>Edificio de 3 Plantas en avenida Sabin y calle publica  Resistencia. 
                <li>Edificio de 4 Plantas en calle J.D Perón 2155 Resistencia.
                <li>Edificio de Departamentos en Paso de la Patria (Corrientes).</li>
                <li>Nuevo Edificio del Colegio Privado José Manuel Estrada. Ubicación: Calle Giachino y Avenida Marconi – Resistencia - Chaco</li>
                <li>Locales Comerciales y Deposito Ubicación:  Circ. II – Sección D – Chacra 278 - Manzana 75 – Parcela 1 – Barranqueras.</li>
                <li>Vivienda Familiar  en Barrio Parque Avalos – Resistencia. 
                <li>Nuevo Edificio del Colegio Privado José Manuel Estrada. Ubicación: Calle Giachino y Avenida Marconi – Resistencia.</li>
            </ul>
            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>NAVES INDUSTRIALES:</strong> 
                se han realizado estudios de suelos para  edificios industriales en la provincia de Chaco, citando las siguientes obras:
            </p>

            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Ampliación y Refacción EASY Resistencia Contratista: ITECH CONSTRUCTORA</li>
                <li>Nave Industrial en J.J Castelli. TBEH EQUIPOS AGRO- INDUSTRIALES.</li>
                <li>PLANTA INDUSTRIAL POLIVALENTE para: ASENSORES SERVAS S.A</li>
            </ul>
            <p data-scrollreveal="enter left after 0s over 3s">
                <strong>ENSAYOS “IN SITU”:</strong> 
                se han realizado estudios de suelos referidos a ensayos de permeabilidad, muestreos y ensayos varios en terrenos y urbanizaciones de la provincia de Chaco y Corrientes, se citan las siguientes obras:
            </p>

            <ul data-scrollreveal="enter right after 0s over 3s">
                <li>Permeabilidad en Lagunas de Tratamiento de Desagües Industriales para Frigorífico FRIGONORTE S.A  en Margarita Belén. </li>
                <li>Permeabilidad en el Proyecto de Lagunas de Tratamiento de Desagües Cloacales en Perugorria – Corrientes.</li>
                <li>Estudio de Suelos para Barrio Privado y Club de Campo “ La Reserva” Av. Soberanía Nacional Y Linch Arribalzaga – Resistencia</li>
                <li>Permeabilidad en vivienda Familiar en Villa Fabiana Norte – Resistencia Comitente: AUDIOCENTER S.R.L.</li>
                <li>Permeabilidad en Edificio de Departamentos en F. Rivadavia Nº 1165 – Resistencia.</li>
                <li>Permeabilidad de pozos absorbentes para desagües cloacales parilla “El Santafesino” R.N Nº 11 Km 1007,8  Resistencia.</li>
                <li>Permeabilidad de pozos absorbentes para Viv. Familiar en Av. Sabin N° 250 –V° Rio Negro - Resistencia</li>
                <li>Muestreo, Clasificación y Compactación y Control de Compactación en 100 Viviendas B° Nalá – P.R Saenz Peña. Contratista: NORESCO S.R.L</li>
                <li>Muestreo, Clasificación y recomendaciobes en 50 Viviendas P.T Digno – Fontana. Contratista: PATAGONIA CONSTRUCCIONES S.R.L</li>
            </ul>
        </div>
    </div>';

