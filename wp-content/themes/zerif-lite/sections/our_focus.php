<?php zerif_before_our_focus_trigger(); ?>

<section class="focus" id="focus">

	<?php zerif_top_our_focus_trigger(); ?>

	<div class="container">

		<!-- SECTION HEADER -->

		<div class="section-header">

			<!-- SECTION TITLE AND SUBTITLE -->

			<?php

			/* Title */
			zerif_our_focus_header_title_trigger();

			/* Subtitle */
			zerif_our_focus_header_subtitle_trigger();

			?>

		</div>

		<div class="row">
				<?php				
                                        
					echo '<div id="carousel-homepage-services" class="carousel slide" data-ride="carousel">';
					
					/* Wrapper for slides */
					
					echo '<div class="carousel-inner" role="listbox">';

					$zerif_latest_loop = new WP_Query(
					  	apply_filters( 'zerif_latest_news_parameters', array('post_type' => 'servicio',
                                                                                                    'order' => 'DESC',
                                                                                                    'ignore_sticky_posts' => true ))
					);
					
					$newSlideActive = '<div class="item active">';
					$newSlide 		= '<div class="item">';
					$newSlideClose 	= '<div class="clear"></div></div>';
					$i_latest_posts= 0;
					
					if ( $zerif_latest_loop->have_posts() ) :
					
					while ( $zerif_latest_loop->have_posts() ) : $zerif_latest_loop->the_post();
					
					$i_latest_posts++;
					
					if ( !wp_is_mobile() ){
					  
					  if($i_latest_posts == 1){
						echo $newSlideActive;
					  }
					  else if($i_latest_posts % 4 == 1){
						echo $newSlide;
					  }
					  
					  
					  the_widget(
							'zerif_ourfocus','title='.get_the_title().'&text=&link='.get_permalink().'&image_uri='.wp_get_attachment_url(get_field('imagen_del_servicio', get_the_ID(), false)),
							array('before_widget' => '', 'after_widget' => '')
					  );
					  
					  
					  /* after every four posts it must closing the '.item' */
					  if($i_latest_posts % 4 == 0){
						echo $newSlideClose;
					  }
					  
					} else {
					  
					  if( $i_latest_posts == 1 ) $active = 'active'; else $active = ''; 
					  
					  echo '<div class="item '.$active.'">';
					  echo '<div class="col-md-3 latestnews-box">';
					  echo '<div class="latestnews-img">';
					  echo '<a class="latestnews-img-a" href="'.get_permalink().'" title="'.get_the_title().'">';
					  if ( has_post_thumbnail() ) :
					  the_post_thumbnail();
					  else:
					  echo '<img src="'.esc_url( get_template_directory_uri() ).'/images/blank-latestposts.png" alt="'.esc_attr( get_the_title() ).'" />';
					  endif; 
					  echo '</a>';
					  echo '</div>';
					  echo '<div class="latesnews-content">';
					  echo '<h3 class="latestnews-title"><a href="'.esc_url( get_permalink() ).'" title="'.esc_attr( get_the_title() ).'">'.wp_kses_post( get_the_title() ).'</a></h3>';
					  
					  $ismore = @strpos( $post->post_content, '<!--more-->');
					  
					  if($ismore) {
						the_content( sprintf( esc_html__('[...]','zerif-lite'), '<span class="screen-reader-text">'.esc_html__('about ', 'zerif-lite').get_the_title().'</span>' ) );
					  } else {
						the_excerpt();
					  }
					  echo '</div>';
					  echo '</div>';
					  echo '</div>';
					}
					
					endwhile;
					
					endif;	
					
					if ( !wp_is_mobile() ) {
					  
					  // if there are less than 10 posts
					  if($i_latest_posts % 4!=0){
						echo $newSlideClose;
					  }
					  
					}
					
					wp_reset_postdata(); 
					
					echo '</div><!-- .carousel-inner -->';
					
					/* Controls */
					echo apply_filters( 'zerif_latest_news_left_arrow','<a class="left carousel-control" href="#carousel-homepage-services" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">'.__('Previous','zerif-lite').'</span>
					</a>' );
					echo apply_filters( 'zerif_latest_news_right_arrow','<a class="right carousel-control" href="#carousel-homepage-services" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">'.__('Next','zerif-lite').'</span>
					</a>' );
					echo '</div><!-- #carousel-homepage-services -->';
					
					echo '</div><!-- .container -->';

				?>

		</div>

	</div> <!-- / END CONTAINER -->

	<?php zerif_bottom_our_focus_trigger(); ?>

</section>  <!-- / END FOCUS SECTION -->

<?php zerif_after_our_focus_trigger(); ?>