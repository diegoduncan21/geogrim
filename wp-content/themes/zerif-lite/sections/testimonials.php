<?php

zerif_before_testimonials_trigger();

echo '<section class="testimonial" id="testimonials">';

	zerif_top_testimonials_trigger();

	echo '<div class="container">';

		echo '<div class="section-header">';

			/* Title */
			zerif_testimonials_header_title_trigger();

			/* Subtitle */
			zerif_testimonials_header_subtitle_trigger();

		echo '</div>';

		echo '<div class="row">';

			echo '<div class="col-md-12">';

				$pinterest_style = '';
				$zerif_testimonials_pinterest_style = get_theme_mod('zerif_testimonials_pinterest_style');
				if( isset($zerif_testimonials_pinterest_style) && $zerif_testimonials_pinterest_style != 0 ) {
					$pinterest_style = 'testimonial-masonry';
				}

				include 'antecedentes.php';
				

                        echo '</div>';

                echo '</div>';

        echo '</div>';
	zerif_bottom_testimonials_trigger();

echo '</section>';

zerif_after_testimonials_trigger();

?>